[@ww.textfield labelKey="hipchat.api.token" name="apiToken" value="${apiToken!}" required=true/]
[@ww.textfield labelKey="hipchat.room" name="room" value="${room!}" required=true/]
[@ww.checkbox labelKey="hipchat.notify" name="notifyUsers" value="${notifyUsers!?string}"/]
